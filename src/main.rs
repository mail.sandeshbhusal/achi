#![allow(unused)]

use core::panic;
use std::collections::{HashSet, VecDeque};

use anyhow::Result;

#[derive(Debug, Hash, PartialEq, Eq, Copy, Clone, Default)]
enum Piece {
    #[default]
    P1,
    P2,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Errors {}

impl std::fmt::Display for Piece {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Piece::P1 => {
                write!(f, "P1")
            }
            Piece::P2 => {
                write!(f, "P2")
            }
        }
    }
}

#[derive(Debug, Hash, Default, Clone, Copy, Eq, PartialEq)]
struct Board {
    next_turn: Piece,
    p1_count: usize,
    p2_count: usize,
    board_state: [Option<Piece>; 9],
}

impl std::fmt::Display for Board {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let cloz = |t: Option<Piece>| t.map_or("..".to_owned(), |t| t.to_string());
        writeln!(
            f,
            "{}|{}|{}",
            cloz(self.board_state[0]),
            cloz(self.board_state[1]),
            cloz(self.board_state[2])
        )?;

        writeln!(
            f,
            "{}|{}|{}",
            cloz(self.board_state[3]),
            cloz(self.board_state[4]),
            cloz(self.board_state[5])
        )?;
        writeln!(
            f,
            "{}|{}|{}",
            cloz(self.board_state[6]),
            cloz(self.board_state[7]),
            cloz(self.board_state[8])
        )?;

        Ok(())
    }
}

impl Board {
    fn new() -> Self {
        Default::default()
    }

    fn is_placement_done(&self) -> bool {
        self.p1_count == 3 && self.p2_count == 3
    }

    pub fn place_position(&mut self, position: u16) -> Result<()> {
        if matches!(self.next_turn, Piece::P1) {
            self.p1_count += 1;
            self.next_turn = Piece::P2;
        } else {
            self.p2_count += 1;
            self.next_turn = Piece::P1;
        }

        if position > 8 {
            anyhow::bail!("Invalid placement position for the piece");
        }

        if self.board_state[position as usize].is_some() {
            anyhow::bail!("The board position {position} is not empty.");
        }

        // Otherwise, place the piece.
        self.board_state[position as usize] = Some(self.next_turn);
        Ok(())
    }

    fn gen_bit_state(&self, piece: Piece) -> u16 {
        // We need 9 bits to encode the entire board position.
        let mut state = 0;

        self.board_state
            .iter()
            .enumerate()
            .filter(|(_, p)| **p == Some(piece))
            .map(|(pos, _)| state |= 1 << pos)
            .count();

        return state;
    }

    fn bit_state_game_over(&self, bitstate: u16) -> bool {
        bitstate & 0b111000000 == 0b111000000
            || bitstate & 0b000111000 == 0b000111000
            || bitstate & 0b000000111 == 0b000000111
            || bitstate & 0b100100100 == 0b100100100
            || bitstate & 0b010010010 == 0b010010010
            || bitstate & 0b001001001 == 0b001001001
            || bitstate & 0b100010001 == 0b100010001
            || bitstate & 0b001010100 == 0b001010100
    }

    // Return if a piece has won a game
    fn check_game_over(&self) -> Option<Piece> {
        let state = self.board_state;

        // Check with piece p1.
        let bit1 = self.gen_bit_state(Piece::P1);
        let bit2 = self.gen_bit_state(Piece::P2);

        if self.bit_state_game_over(bit1) {
            return Some(Piece::P1);
        }

        if self.bit_state_game_over(bit2) {
            return Some(Piece::P2);
        }

        None
    }

    fn move_piece(&mut self, from: usize, to: usize) -> Result<()> {
        if self.board_state[from].is_none() {
            return anyhow::bail!("Source position is empty.");
        }

        if self.board_state[to].is_some() {
            return anyhow::bail!("Destination position is not empty.");
        }

        // Actually move the piece.
        let source_piece = self.board_state[from].clone();
        self.board_state[from] = None;
        self.board_state[to] = source_piece;

        // Reset player turn.
        if matches!(self.next_turn, Piece::P1) {
            self.next_turn = Piece::P2;
        } else {
            self.next_turn = Piece::P1;
        }
        Ok(())
    }

    // Get all positions the piece in "position" can move into.
    fn get_possible_movement_from(&self, pos: usize) -> Vec<usize> {
        match pos {
            0 => {
                vec![1, 3, 4]
            }
            1 => {
                vec![0, 2, 4]
            }
            2 => {
                vec![1, 4, 5]
            }
            3 => {
                vec![0, 4, 6]
            }
            4 => {
                vec![0, 1, 2, 3, 5, 6, 7, 8]
            }
            5 => {
                vec![2, 4, 8]
            }
            6 => {
                vec![3, 4, 7]
            }
            7 => {
                vec![6, 4, 8]
            }
            8 => {
                vec![4, 5, 7]
            }
            _ => {
                panic!("Invalid board point.")
            }
        }
    }

    /// Given a board state, this function generates all valid "move" combinations
    /// formatted as Vec<(source, destination)>. For e.g. if board next turn is P2,
    /// it generates all pieces that P2 can move on the board.
    fn get_move_positions(&self) -> Vec<(usize, usize)> {
        // Find where next-piece pieces are placed.
        let next_turn = self.next_turn;
        let current_placements = self
            .board_state
            .iter()
            .enumerate()
            .filter_map(move |(pos, item)| {
                if matches!(item, next_turn) {
                    Some(pos)
                } else {
                    None
                }
            })
            .collect::<Vec<usize>>();

        // get board empty spots.
        let empty = self.get_empty_positions();
        let mut moves: Vec<(usize, usize)> = vec![];
        for piece_position in current_placements {
            let possible_move_from = piece_position;
            let possible_moves_to = self.get_possible_movement_from(possible_move_from);
            for possible_move_to in possible_moves_to {
                moves.push((possible_move_from, possible_move_to));
            }
        }

        moves
    }

    fn get_empty_positions(&self) -> Vec<u16> {
        self.board_state
            .iter()
            .enumerate()
            .filter_map(|(p, k)| if k.is_none() { Some(p as u16) } else { None })
            .collect::<Vec<u16>>()
    }

    // Invalid winning state
    fn is_invalid_state(&self) -> bool {
        let state = self.board_state;

        // Check with piece p1.
        let bit1 = self.gen_bit_state(Piece::P1);
        let bit2 = self.gen_bit_state(Piece::P2);

        if self.bit_state_game_over(bit1) && self.bit_state_game_over(bit2) {
            true
        } else {
            false
        }
    }
}

fn main() {
    bfs_game();
}

#[cfg(test)]
mod tests {
    use crate::{Board, Piece};

    #[test]
    fn test_simple_game() {
        let mut board = Board::new();
        assert_eq!(board.check_game_over(), None);
        board.place_position(0);
        assert!(board.place_position(0).is_err());
        board.place_position(1);
        board.place_position(3);
        board.place_position(2);
        assert_eq!(board.check_game_over(), Some(Piece::P1));
    }
}

// Run a breadth-first search on the board states, starting with an empty state.
// The queue gets populated each time, with more states, until they are valid, and the
// integer count also increases.
fn bfs_game() {
    let mut board = Board::new();
    let mut queue = VecDeque::<Board>::new();
    let mut total_invalid = 0;
    let mut total_possible = 0;

    // Starting position can be anything.
    for i in 0..9 {
        let mut cloned_p1 = board.clone();
        cloned_p1.place_position(i);
        queue.push_back(cloned_p1);
    }

    let mut p1wins = 0;
    let mut p2wins = 0;

    // Check initial number of valid/invalid states.
    let mut playqueue: VecDeque<Board> = VecDeque::new();

    while let Some(mut state) = queue.pop_front() {
        // If all points are placed, we check if game is over.
        // If game is not over, we can continue to play the game from here.
        if state.is_placement_done() {
            total_possible += 1;
            match state.check_game_over() {
                Some(_) => continue,
                None => {
                    playqueue.push_back(state.clone());
                    continue;
                }
            }
        }

        let positions = state.get_empty_positions();
        for position in positions {
            // Generate a new state.
            let mut cloned_state = state.clone();
            cloned_state.place_position(position);
            queue.push_back(cloned_state);
        }
    }
    // We can drop the queue from memory to reduce mem footprint ;)
    drop(queue);

    // Add all "seen" states to the seen_states queue.
    let mut seen_states: HashSet<Board> = HashSet::new();
    let mut seen_count = 2;
    let mut unseen_count = 0;
    let mut p1wins = 0;
    let mut p2wins = 0;
    let mut additions = vec![];

    while let Some(state) = playqueue.pop_front() {
        if let Some(piece) = state.check_game_over() {
            println!("{} wins the game!", piece);
            println!("{}", state);
            println!("-----");
            if matches!(piece, Piece::P1) {
                p1wins += 1;
            } else {
                p2wins += 1;
            }
            continue;
        }

        if seen_states.contains(&state) {
            // This state was already seen before.
            // This is a loopback state, and hence,
            // we do not explore it further.
            seen_count += 1;
            continue;
        }

        // This state is effectively "seen" and will not be explored further.
        seen_states.insert(state);
        // println!("{}", state);
        unseen_count += 1;

        // Generate all possible new states for the board state.
        let moves = state.get_move_positions();
        additions.push(moves.len()); // how many states were added by this move.
        for _move in moves {
            // println!("In state {}, moving {} to {}", state, _move.0, _move.1);
            let mut new_state = state.clone();
            new_state.move_piece(_move.0, _move.1);
            playqueue.push_back(new_state);
        }
    }

    let total_moves_added: usize = additions.iter().sum();
    let state_count = additions.len();
    let branching_factor = total_moves_added as f64 / (seen_count + unseen_count) as f64;

    dbg!(&playqueue.len());
    dbg!(&seen_count);
    dbg!(&unseen_count);
    dbg!(&branching_factor);
    dbg!(&p1wins);
    dbg!(&p2wins);
}
