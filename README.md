# Analysing Achi.

This project is done to check some assertions made in the paper about Achi which I wrote as an assignment for class of 
Artificial Intelligence (grad) at Boise State University (Fall 2023). I analyse Achi in this project, and determine a few
things like:

- Branching factor
- Playqueue length
- Branching factor during placement
- Placement possible states
- Number of wins of each player

In order to run just the placement phase of the game to analyse data, checkout to commit id `a8858727` and play around. For this, run `git checkout a8858727` in your terminal.

To run this entire game with BFS search, just run `cargo run --release` inside this folder. Please have Rust installed before running this project, as
it is written entirely in Rust.

Suggestions and issues are very welcome.